// src/cdr/model.rs
use crate::api_error::ApiError;
use crate::db;
use crate::schema::cdrs;
use chrono::{NaiveDateTime};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
// use uuid::Uuid;


#[derive(Serialize, Deserialize, Identifiable, Queryable)]
#[table_name = "cdrs"]
pub struct CDR {
    pub id: i64,
    pub agent_id: i64,
    pub account_id: i64,
    pub direction: String, // TODO: create enum
    pub duration: f64,
    pub pots: String, // TODO: implement a phone number type
    pub dnis: String,
    pub ani: String,
    pub created_at: NaiveDateTime,
    pub updated_at: Option<NaiveDateTime>,
}

#[derive(Deserialize, Insertable)]
#[table_name = "cdrs"]
pub struct NewCDR {
    pub agent_id: i64,
    pub account_id: i64,
    pub direction: String, // TODO: create enum
    pub duration: f64,
    pub pots: String, // TODO: implement a phone number type
    pub dnis: String,
    pub ani: String,
}


#[derive(Deserialize, AsChangeset)]
#[table_name = "cdrs"]
pub struct CDRChanges {
    pub agent_id: Option<i64>,
    pub account_id: Option<i64>,
    pub direction: Option<String>, // TODO: create enum
    pub duration: Option<f64>,
    pub pots: Option<String>, // TODO: implement a phone number type
    pub dnis: Option<String>,
    pub ani: Option<String>,
}


impl CDR {
    pub fn find_all() -> Result<Vec<Self>, ApiError> {
        let conn = db::connection()?;

        let cdrs = cdrs::table
            .load::<CDR>(&conn)?;

        Ok(cdrs)
    }

    pub fn find(id: i64) -> Result<Self, ApiError> {
        let conn = db::connection()?;

        let cdr = cdrs::table
            .filter(cdrs::id.eq(id))
            .first(&conn)?;

        Ok(cdr)
    }

    pub fn create(ncdr: NewCDR) -> Result<Self, ApiError> {
        let conn = db::connection()?;

        let cdr = diesel::insert_into(cdrs::table)
            .values(ncdr)
            .get_result(&conn)?;

        Ok(cdr)
    }

    pub fn update(id: i64, cdr_changes: CDRChanges) -> Result<Self, ApiError> {
        let conn = db::connection()?;

        let cdr = diesel::update(cdrs::table)
            .filter(cdrs::id.eq(id))
            .set(cdr_changes)
            .get_result(&conn)?;

        Ok(cdr)
    }

    pub fn delete(id: i64) -> Result<usize, ApiError> {
        let conn = db::connection()?;

        let res = diesel::delete(
            cdrs::table
                    .filter(cdrs::id.eq(id))
            )
            .execute(&conn)?;

        Ok(res)
    }
}
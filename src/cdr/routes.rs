// src/cdr/routes.rs
use crate::api_error::ApiError;
use crate::cdr::{CDR, NewCDR, CDRChanges};
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::json;
// use uuid::Uuid;

#[get("/cdrs")]
async fn find_all() -> Result<HttpResponse, ApiError> {
    let cdrs = CDR::find_all()?;
    Ok(HttpResponse::Ok().json(cdrs))
}

#[get("/cdrs/{id}")]
async fn find(id: web::Path<i64>) -> Result<HttpResponse, ApiError> {
    let cdr = CDR::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(cdr))
}

#[post("/cdrs")]
async fn create(cdr: web::Json<NewCDR>) -> Result<HttpResponse, ApiError> {
    let cdr = CDR::create(cdr.into_inner())?;
    Ok(HttpResponse::Ok().json(cdr))
}

// #[put("/cdrs/{id}")]
// async fn replace(id: web::Path<i64>, cdr: web::Json<CDRChanges>) -> Result<HttpResponse, ApiError> {
//     let cdr = CDR::replace(id.into_inner(), cdr.into_inner())?;
//     Ok(HttpResponse::Ok().json(cdr))
// }

#[put("/cdrs/{id}")]
async fn update(id: web::Path<i64>, cdr: web::Json<CDRChanges>) -> Result<HttpResponse, ApiError> {
    let cdr = CDR::update(id.into_inner(), cdr.into_inner())?;
    Ok(HttpResponse::Ok().json(cdr))
}

#[delete("/cdrs/{id}")]
async fn delete(id: web::Path<i64>) -> Result<HttpResponse, ApiError> {
    let num_deleted = CDR::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted": num_deleted })))
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(find_all);
    cfg.service(find);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}
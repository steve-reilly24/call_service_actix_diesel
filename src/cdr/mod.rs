// src/user/mod.rs
mod model;
mod routes;

pub use model::{CDR, NewCDR, CDRChanges};
pub use routes::init_routes;

table! {
    cdrs (id) {
        id -> Int8,
        agent_id -> Int8,
        account_id -> Int8,
        direction -> Text,
        duration -> Float8,
        pots -> Text,
        dnis -> Text,
        ani -> Text,
        created_at -> Timestamp,
        updated_at -> Nullable<Timestamp>,
    }
}

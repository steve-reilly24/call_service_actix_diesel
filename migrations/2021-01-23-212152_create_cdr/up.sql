-- up.sql
CREATE TABLE cdrs (
    id BIGSERIAL PRIMARY KEY,
    agent_id BIGINT NOT NULL,
    account_id BIGINT NOT NULL,
    direction TEXT NOT NULL,
    duration FLOAT NOT NULL,
    pots TEXT NOT NULL,
    dnis TEXT NOT NULL,
    ani TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP
);
SELECT diesel_manage_updated_at('cdrs');